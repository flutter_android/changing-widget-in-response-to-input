import 'package:flutter/material.dart';

// StatefulWidget ควบคุมการปปข้อมูล
class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}

//เป็นstate ของ class Counter
class _CounterState extends State<StatefulWidget> {
  int _counter = 0;
  void _increment() {
    //update Text
    setState(() {
      _counter++;
    });

    print(_counter);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(onPressed: _increment, child: Text('Increment')),
        SizedBox(
          width: 16,
        ),
        Text('Counter: $_counter'),
      ],
    );
  }
}

void main() {
  runApp(MaterialApp(
    title: 'CounterState',
    home: Scaffold(
      body: Center(
        child: Counter(),
      ),
    ),
  ));
}
